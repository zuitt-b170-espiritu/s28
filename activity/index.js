const http = require("http")

const port = 4000

const server = http.createServer((req, res)=>{
	if (req.url === "/login") {
		res.writeHead(200, {'Content-Type': 'text/plain'})
		res.end('Welcome to the login page')

		
	}
	else{
		res.writeHead (404, {'Content-Type': 'text/plain'});
		res.end(`I'm sorry. The page you are looking for cannot be found	`)
	}
})
server.listen(port)

console.log(`server now accessible at localhost:${port}`)





// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.
/*fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())
.then((json)=>console.log(json));
*/


/*fetch("https://jsonplaceholder.typicode.com/todos")
.then((response)=>response.json())

.then((json)=>console.log(json[2]));*/



/*fetch("https://jsonplaceholder.typicode.com/todos",{
	method:"GET",
	headers:{
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		userId:1,
		title:"New Post",
		body:"Hello World"
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));
*/

// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.
/*fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=>response.json())
.then((json)=>console.log(json));*/


// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.
/*fetch("https://jsonplaceholder.typicode.com/todos/1")
.then((response)=>response.json())
.then((json)=>console.log(json[2,3]));*/




// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.
/*fetch("https://jsonplaceholder.typicode.com/todos",{
	method: "POST",
	headers: {
		"Content-Type": "application/json"
	},

	body:JSON.stringify({
		title: "New Post",
		completed:"Hello World"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));*/



// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.
/*fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title:"To do list",
		description:"activity",
		status:"completed",
		dateCompleted:"april 21, 2022",
		userID:1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));*/


// 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.
// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

/*fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PATCH",
	headers: {
		"Content-Type": "application/json"
	},
	body:JSON.stringify({
		title:"Updated to do list",
		description:"activity s28",
		status:"completed",
		dateCompleted:"april 22, 2022",
		userID:1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));*/



// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.
/*fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method:"DELETE",
})*/