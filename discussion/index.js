// SYNCHRONOUS VS JS ASYNCHRONOUS
// Synchronous programming - only one statement/line of codes is being processed at a time:
// being used by the javascript


// the error checking proves the synchronous programming of javascript since the after detecting the error, the next lines of codes will not be executed even if they have no errors
console.log("Hello World");
console.log("Hello Again");
console.log("Goodbye");


// when certain statements take a lot of time to process, this slows down the running/executing of code
// an example of this is when loops are used on a large amount of information or when fetching data from database
// when an action will take some time to be executed, this results in "code blocking"
	// code blocking - delaying of a more efficient code compared to ones currently executed
console.log("Hello World");
// we might not notice it due to improved processing power of our devices, but the process of fetching/using large amounts of information in our loops takes too much compared to logging "Hello Again"
// another example is when you try to access a website and it takes a while to load(white webpage is being displayed before the landing page is loaded)
for(let i=0;i<=1500;i++){
	console.log(i)
}
console.log("Hello Again");




/*
JAVASCRIPT ASYNCHRONOUS

*/

// FETCH function fetch API
// a "promise" is an object that represents eventual completion (or failure) of an asynchronous function and its resulting
// SYNTAX
/*
	fetch('URL')
*/

console.log(fetch("https://jsonplaceholder.typicode.com/post"));






/*
	SYNTAX:
		fetch(url).then((parameter)=>statement).then((paramerer)=>statement)

*/
// retrieves all posts following the REST API method (read/GET)
// by using the .then method, we can now check the status of the promise
fetch("https://jsonplaceholder.typicode.com/posts")

// since fetch method returns a "promise", the "then" method will catch the promise and make it the "response" object
// also, the then method captures the "response" object and returns another promise which will eventually be rejected/resolved
.then(response=>console.log(response.status));


// this will logged first, before the status of the promise due to javascript asynchronous
console.log("Hello Again");


fetch("https://jsonplaceholder.typicode.com/posts")
// the use of "json()" is to convert the response object into json format to be used by the application
.then((response)=>response.json())
// since we cannot directly print the json format of the response in the seconde .then method, we need another .then method to catch the promise and print the "response.json()"
// which is being represented by the "json" parameter
// using them methods multiple times would create promise chains
.then((json)=>console.log(json));



// ASYNC-AWAIT
// async-await keywords is another approach that can be used to achieve js asynchronous
// used in functions to indicate which portions of code should be waited
// the codes outside the functions will be executed under asynchronous
async function fetchData(){
	// waits for the fetch method to be done before storing value of the respnse in the "result" variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts")
	console.log(result);
	console.log(typeof result);
	// we cannot access the body of the result
	console.log(result.body);
	// converts the data from the "result" variable and stores it in "json" variable
	let json=await result.json();
	console.log(json);
}
fetchData();
console.log("Hello Again");


/*
miniactivity
using the then method, retrieve the first object in the jsonplaceholder url
	the response should be converted first into json format before displaying in the console
*/

fetch("https://jsonplaceholder.typicode.com/posts/1")
.then((response)=>response.json())

.then((json)=>console.log(json[0]));


// CREATE/UPDATE A RESOURCE
/*
	SYNTAX
	fetch("url",{options}, details of the request body)
	.then(response=>{})
	.then((json)=>{})
*/
// using post method to create object (posts/:id, POST)
fetch("https://jsonplaceholder.typicode.com/posts",{
	method:"POST",
	headers:{
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		userId:1,
		title:"New Post",
		body:"Hello World"
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


/*
create another fetch request (the url should contain 1 as the id endpoint) with PUT method
	just only specify the title
		title:corrected post
*/
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method:"PUT",
	headers:{
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
		// userId:1,
			title:"Corrected Post",
		// body:"Hello World"
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));




/*
	PUT - replaces the whole object
	PATCH - updates the specified key/s
*/
/*
create another fetch requesst(the url should contain 1 as the id endpoint) with PATCH method
	userId:1
	title:Updated Post
*/

fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method:"PATCH",
	headers:{
		"Content-Type":"application/json"
	},
	body:JSON.stringify({
			userId:1,
			title:"Updated Post",
		// body:"Hello World"
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));



// deleting a resources
fetch("https://jsonplaceholder.typicode.com/posts/1",{
	method:"DELETE",
})



/*
Filtering Posts
*/
/*
	the data/result coming from the fetch method can be filtered by sending a key-value pair along with its URL

	the information is sent via the url can be done by adding the question mark symbol(?)

	SYNTAX
		-"url?parameterName=value"
		-"url?paramA=valueA&paramB=valueB"
*/

fetch('https://jsonplaceholder.typicode.com/posts?userId=1&id=3')
.then((response)=>response.json())
.then((json)=>console.log(json));


/*
retrieve the nested comments array in the first entry using get method
*/
// Retrieving nested/related comments to posts
// GET Method url/posts/:id/comments
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=>response.json())
.then((json)=>console.log(json));